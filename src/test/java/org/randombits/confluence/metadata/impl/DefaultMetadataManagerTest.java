package org.randombits.confluence.metadata.impl;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.confluence.core.ContentEntityManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.randombits.confluence.metadata.MetadataStorage;
import org.randombits.confluence.metadata.TypeHandler;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/**
 * @author William Tan WeiChun
 * @since 7.2.0.20150414
 */
@RunWith(MockitoJUnitRunner.class)
public class DefaultMetadataManagerTest {

    DefaultMetadataManager $;

    @Mock EventPublisher eventPublisher;
    @Mock CacheManager cacheManager;
    @Mock PluginAccessor pluginAccessor;
    @Mock PluginEventManager pluginEventManager;
    @Mock ContentPropertyManager contentPropertyManager;
    @Mock ContentEntityManager contentEntityManager;

    @Mock MetadataStorage metadataStorage;
    @Mock ContentEntityObject contentEntityObject;
    @Mock Cache cache;
    @Mock TypeHandler typeHandler;

    @Before public void setUp() {
        $ = new DefaultMetadataManager(cacheManager, pluginAccessor, pluginEventManager, contentPropertyManager, contentEntityManager);
        when(metadataStorage.getContent()).thenReturn(contentEntityObject);
        when(contentEntityObject.getLatestVersion()).thenReturn(contentEntityObject);
        when(cacheManager.getCache(anyString())).thenReturn(cache);
    }

    @Test public void shouldLoadLatestDataXml() {
        // Latest content version is 5 while latest metadata version is 3.
        when(contentPropertyManager.getTextProperty(contentEntityObject, "~metadata.3")).thenReturn(randomAlphanumeric(4));
        when(contentEntityObject.getVersion()).thenReturn(5);
        assertNotNull($.loadDataXML(contentEntityObject));
    }

    @Test public void shouldReturnNullIfNoDataXmlFound() {
        when(contentEntityObject.getVersion()).thenReturn(5);
        assertNull($.loadDataXML(contentEntityObject));
    }
}
