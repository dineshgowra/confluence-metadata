/*
 * Copyright (c) 2005, David Peterson 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 * 			 notice, this list of conditions and the following disclaimer in the
 *   		 documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 * 			 may be used to endorse or promote products derived from this software
 * 			 without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * v1.0: Created on 16/05/2005 by David Peterson
 */
package org.randombits.confluence.metadata.reference;

import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.user.User;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.HashCodeBuilder;

import static com.atlassian.confluence.user.persistence.dao.compatibility.FindUserHelper.getUserByUsername;

/**
 * @author randomeizer (David Peterson)
 * @author yclian
 */
public class UserReference implements Reference<User> {

    private static final long serialVersionUID = 1934030029425758042L;

    private String username;
    private String userKey;

    /**
     * Constructs a new, blank user option. For use when defrosting.
     */
    protected UserReference() {
    }

    public UserReference(ConfluenceUser user) {
        this(user.getKey());
    }

    /**
     * @deprecated Since 6.0.0 as Confluence 5.3 now uses userKey, not username.
     * @param username
     */
    @Deprecated
    public UserReference(String username) {
        this.username = username;
        // This won't happen when UserReference is being constructed through reflection as field injection is used.
        this.userKey = getUserKeyFromUsername(username);
    }

    /**
     * Constructs a new user option.
     *
     * @param userKey the user key of the user
     */
    public UserReference(UserKey userKey) {
        this.userKey = userKey.toString();
    }

    private String getUserKeyFromUsername(String username) {
        ConfluenceUser user = getUserByUsername(username);
        return null != user ? user.getKey().getStringValue() : null;
    }

    /**
     * @deprecated Since 6.0.0. Use {@link #getUserKey()} instead.
     * @return
     */
    @Deprecated
    public String getUsername() {
        return username;
    }

    public String getUserKey() {
        return userKey;
    }

    /**
     * @param obj The object to compare.
     * @return <code>true</code> if the object is a UserOption with the same
     *         username.
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals( Object obj ) {
        if (obj instanceof UserReference) {
            UserReference uo = (UserReference) obj;
            return StringUtils.equals(userKey, uo.userKey);
        }
        return false;
    }

    /**
     * @return the hashcode for the object.
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(userKey).toHashCode();
    }

    /**
     * @return the String version of the list option.
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {

        StringBuilder r = new StringBuilder("user: ");

        if (null == userKey) {
            // HACK [20140208 YCL] Return the legacy username value if userKey is null (to make troubleshooting easier).
            // I don't know what would be the impact, no client should use toString() I suppose?
            if (null != username) {
                r.append(username);
            } else {
                r.append("null");
            }
        } else {
            r.append(userKey.toString());
        }

        return r.toString();
    }
}
