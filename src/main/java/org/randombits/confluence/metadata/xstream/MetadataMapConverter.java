/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.confluence.metadata.xstream;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.converters.collections.MapConverter;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.mapper.Mapper;
import org.randombits.confluence.metadata.MetadataMap;
import org.randombits.confluence.metadata.Placeholder;

/**
 * An extension of the standard
 * {@link com.thoughtworks.xstream.converters.collections.MapConverter} which
 * will replace any unrecognised entries with an
 * {@link org.randombits.confluence.metadata.Placeholder}.
 *
 * @author David Peterson
 */
public class MetadataMapConverter extends MapConverter {
    public MetadataMapConverter(Mapper mapper) {
        super(mapper);
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean canConvert(Class type) {
        return MetadataMap.class.equals(type);
    }


    //  TODO: Re-enable this once Confluence has upgraded to XStream 1.2
    protected void writeItem(Object item, MarshallingContext context,
                             HierarchicalStreamWriter writer) {
        // PUBLISHED API METHOD! If changing signature, ensure backwards compatability.
        if (item == null) {
            // todo: this is duplicated in TreeMarshaller.start()
            String name = mapper().serializedClass(null);
            writer.startNode(name);
            writer.endNode();
        } else {
            String name;

            if (item instanceof Placeholder) {
                name = ((Placeholder) item).getName();
            } else {
                name = mapper().serializedClass(item.getClass());
            }

            writer.startNode(name);
            context.convertAnother(item);
            writer.endNode();
        }
    }

    protected Object readItem(HierarchicalStreamReader reader,
                              UnmarshallingContext context, Object current) {
        // PUBLISHED API METHOD! If changing signature, ensure backwards compatability.
        String classAttribute = reader.getAttribute("class");
        Class type;

        try {
            if (classAttribute == null) {
                type = mapper().realClass(reader.getNodeName());
            } else {
                type = mapper().realClass(classAttribute);
            }
        } catch (Exception e) {
            // com.thoughtworks.xstream.alias.CannotResolveClassException was removed
            // after com.thoughtworks.xstream:xstream:jar:1.1.1
            type = Placeholder.class;
        }
        return context.convertAnother(current, type);
    }

}
