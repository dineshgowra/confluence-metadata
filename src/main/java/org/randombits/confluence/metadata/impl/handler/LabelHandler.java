package org.randombits.confluence.metadata.impl.handler;

import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import org.randombits.confluence.metadata.AbstractTypeHandler;
import org.randombits.confluence.metadata.reference.LabelReference;

/**
 * Handles converting {@link Label}s to {@link org.randombits.confluence.metadata.reference.LabelReference}s and vice versa.
 */
public class LabelHandler extends AbstractTypeHandler<Label, LabelReference> {

    public static final String ALIAS = "LabelReference";

    private final LabelManager labelManager;

    public LabelHandler( LabelManager labelManager ) {
        super( Label.class, LabelReference.class );
        this.labelManager = labelManager;
    }

    @Override
    protected Label doGetOriginal( LabelReference stored ) {
        Label label = labelManager.getLabel( stored.getLabelName() );
        if ( label == null ) {
            label = new Label( stored.getLabelName() );
            label = labelManager.createLabel( label );
        }

        return label;
    }

    @Override
    protected LabelReference doGetStored( Label original ) {
        return new LabelReference( original );
    }

    @Override
    protected String getAlias() {
        return ALIAS;
    }
}
