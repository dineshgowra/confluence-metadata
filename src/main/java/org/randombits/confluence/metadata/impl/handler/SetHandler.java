package org.randombits.confluence.metadata.impl.handler;

import org.randombits.confluence.metadata.MetadataManager;
import org.randombits.confluence.metadata.TypeConversionException;
import org.randombits.confluence.metadata.TypeHandler;

import java.util.HashSet;
import java.util.Set;

/**
 * Supports transforming set contents.
 */
public class SetHandler implements TypeHandler {

    private final MetadataManager metadataManager;

    public SetHandler(MetadataManager metadataManager) {
        this.metadataManager = metadataManager;
    }

    public boolean supportsOriginal( Object original ) {
        return original instanceof Set;
    }

    public boolean supportsStorable( Object stored ) {
        return stored instanceof Set;
    }

    public Object getOriginal( Object stored ) throws TypeConversionException {
        Set<?> storedList = (Set<?>) stored;
        Set<Object> originalSet = new HashSet<Object>();
        for ( Object value : storedList ) {
            originalSet.add( metadataManager.fromStorable( value ) );
        }
        return originalSet;
    }

    public Object getStorable( Object original ) throws TypeConversionException {
        Set<?> originalSet = (Set<?>) original;
        Set<Object> storedSet = new HashSet<Object>();
        for ( Object value : originalSet ) {
            storedSet.add( metadataManager.toStorable( value ) );
        }
        return storedSet;
    }
}
