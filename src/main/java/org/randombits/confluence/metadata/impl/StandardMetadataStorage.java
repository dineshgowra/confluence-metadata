/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.randombits.confluence.metadata.impl;

import com.atlassian.confluence.core.ContentEntityObject;
import org.apache.commons.lang.ObjectUtils;
import org.randombits.confluence.metadata.MetadataManager;
import org.randombits.confluence.metadata.MetadataStorage;
import org.randombits.confluence.metadata.Notifiable;
import org.randombits.confluence.metadata.TypeConversionException;
import org.randombits.storage.MapStorage;

import java.util.Map;

/**
 * This is the most common instance of metadata storage. It provides access to
 * the standard metadata information.
 */
public class StandardMetadataStorage extends MapStorage implements MetadataStorage {

    private ContentEntityObject content;

    private boolean modified = false;

    private boolean readOnly;

    private final MetadataManager metadataManager;

    /**
     * @param content  The content object.
     * @param base     The Map serving as the base storage location.
     * @param readOnly If <code>true</code>, the storage will be read-only.
     * @param metadataManager
     */
    StandardMetadataStorage(MetadataManager metadataManager, ContentEntityObject content, Map<String, Object> base, boolean readOnly) {
        super( base );
        this.content = content;
        this.readOnly = readOnly;
        this.metadataManager = metadataManager;
    }

    /**
     * @return the ContentEntityObject the metadata is for.
     */
    public ContentEntityObject getContent() {
        return content;
    }

    /**
     * Returns <code>true</code> if the metadata has been modified since it
     * was last loaded/created.
     *
     * @return <code>true</code> if modified.
     */
    public boolean isModified() {
        return modified;
    }

    @Override
    protected <T> T toType( Object value, T def, Class<T> clazz ) {
        if ( value != null ) {
            Object original = null;
            try {
                original = metadataManager.fromStorable( value );
                if ( original == null ) {
                    //throw new IllegalArgumentException( "Unsupported type: " + value.getClass() );
                    return null;
                }
                value = original;
            } catch ( TypeConversionException e ) {
                throw new IllegalArgumentException( e.getMessage(), e );
            }
        }

        if ( clazz.isInstance( value ) ) {
            return (T) value;
        }
        return def;
    }

    /**
     * Added support for
     */
    @Override
    protected void setBaseObject( String name, Object value ) {
        Object current = getBaseObject( name );
        if ( !ObjectUtils.equals( current, value ) ) {
            // Notify any outgoing objects of their removal.
            if ( current instanceof Notifiable ) {
                ( (Notifiable) current ).removalNotification( name );
            }

            try {
                value = metadataManager.toStorable( value );
            } catch ( TypeConversionException e ) {
                throw new IllegalArgumentException( e.getMessage(), e );
            }
            super.setBaseObject( name, value );

            // Notify the incoming value of it's storage.
            if ( value instanceof Notifiable ) {
                ( (Notifiable) value ).storageNotification( name );
            }

            modified = true;
        }
    }

    @Override
    public boolean isReadOnly() {
        if ( readOnly )
            return true;
        return super.isReadOnly();
    }

}
