package org.randombits.confluence.metadata.impl.handler;

import com.atlassian.confluence.core.ContentEntityManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import org.randombits.confluence.metadata.AbstractTypeHandler;
import org.randombits.confluence.metadata.MetadataManager;
import org.randombits.confluence.metadata.MetadataStorage;
import org.randombits.confluence.metadata.impl.StandardMetadataStorage;
import org.randombits.confluence.metadata.migration.AttachmentReferenceMigrator;
import org.randombits.confluence.metadata.reference.AttachmentReference;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * Handles Attachments.
 */
public class AttachmentHandler extends AbstractTypeHandler<Attachment, AttachmentReference> {

    public static final String LEGACY_ALIAS = "AttachmentOption";

    public static final String ALIAS = "AttachmentReference";

    private final AttachmentManager attachmentManager;
    private final ContentEntityManager contentEntityManager;
    private final MetadataManager metadataManager;
    private final AttachmentReferenceMigrator attachmentReferenceMigrator;

    public AttachmentHandler(
            AttachmentManager attachmentManager,
            @Qualifier("contentEntityManager") ContentEntityManager contentEntityManager,
            MetadataManager metadataManager,
            AttachmentReferenceMigrator attachmentReferenceMigrator) {
        super(Attachment.class, AttachmentReference.class);
        this.attachmentManager = attachmentManager;
        this.contentEntityManager = contentEntityManager;
        this.metadataManager = metadataManager;
        this.attachmentReferenceMigrator = attachmentReferenceMigrator;
    }

    @Override
    protected Attachment doGetOriginal(AttachmentReference stored) {
        ContentEntityObject content = contentEntityManager.getById(stored.getContentId());
        return content != null ? retrieveAttachment(content, stored) : null;
    }

    @Override
    protected AttachmentReference doGetStored(Attachment original) {
        return new AttachmentReference(original);
    }

    @Override
    protected String getAlias() {
        return ALIAS;
    }

    private Attachment retrieveAttachment(ContentEntityObject content, AttachmentReference attachmentReference) {
        Attachment attachment = attachmentManager.getAttachment(attachmentReference.getAttachmentId());
        if (attachment == null) {
            // Attachments that use filename as identifier prior to 6.1.0 need to be migrated.
            attachment = attachmentManager.getAttachment(content, attachmentReference.getFileName());
            if (attachment != null) {
                migrateAttachment(attachment);
            }
        }
        return attachment;
    }

    private void migrateAttachment(Attachment attachment) {
        MetadataStorage metadataStorage = metadataManager.loadWritableData(attachment.getContent());
        if (metadataStorage instanceof StandardMetadataStorage) {
            // To ensure that metadataStorage is not EmptyMetadataStorage.
            StandardMetadataStorage standardMetadataStorage = (StandardMetadataStorage) metadataStorage;
            attachmentReferenceMigrator.process(standardMetadataStorage.getBaseMap(), attachment);
            metadataManager.saveData(standardMetadataStorage, /* override current version */ true);
        }
    }
}
