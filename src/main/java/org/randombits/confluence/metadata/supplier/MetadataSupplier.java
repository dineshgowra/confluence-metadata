/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.confluence.metadata.supplier;

import com.atlassian.confluence.core.ContentEntityObject;
import org.randombits.confluence.metadata.MetadataManager;
import org.randombits.confluence.metadata.MetadataStorage;
import org.randombits.storage.Storage;
import org.randombits.storage.StorageUtils;
import org.randombits.supplier.core.annotate.*;

import java.util.List;
import java.util.Map;

/**
 * Supplies scaffold data values to other plugins.
 *
 * @author David Peterson
 */
@SupplierPrefix(value = "data", required = true)
@SupportedTypes({ContentEntityObject.class, Map.class})
public class MetadataSupplier extends AnnotatedSupplier {

    private static final String DATA_PREFIX = "data";

    private final MetadataManager metadataManager;

    public MetadataSupplier( MetadataManager metadataManager ) {
        this.metadataManager = metadataManager;
    }


    /**
     * Finds the specified value given the context.
     *
     * @param context The context object.
     * @param key     The key of the value to find.
     * @return The value, or the <code>null</code> if none was found for the
     *         specified key.
     */
    @SupplierKey("{key}")
    public Object getValue( @KeyValue Object context, @KeyParam("key") String key ) {
        Storage data = null;
        if ( context instanceof ContentEntityObject ) {
            ContentEntityObject content = (ContentEntityObject) context;
            data = metadataManager.loadWritableData( content );
        } else if ( context instanceof Map<?, ?> ) {
            data = metadataManager.asMetadataStorage( (Map<String, Object>) context );
        }

        if ( data != null ) {
            Object value = StorageUtils.findObject( data, key, null );
            if ( value instanceof Map<?, ?> )
                value = handleMap( (Map<String, Object>) value );

            return value;
        }
        return null;
    }
    

    private Object handleMap( Map<String, Object> map ) {
        Object countObject = map.get( MetadataStorage.ROW_COUNT_FIELD );
        
        if ( countObject instanceof Integer ) {
            List<Object> list = new java.util.ArrayList<Object>();

            int count = ( (Integer) countObject ).intValue();
            
            for ( int i = 0; i < count; i++ ) {
                list.add( map.get( String.valueOf( i ) ) );
            }
            
            return list;
        } else {
            return map;
        }
    }
    /*
    @SupplierKey("{key}")
    @API("1.0.0")
    public String getAllEntries(@KeyValue Map<?, ?> map, @KeyParam("key") String key) {
        return (String) map.get(key);
    }
 */
}
