package org.randombits.confluence.metadata;

import org.randombits.storage.Aliasable;

/**
 * Indicates that the class has an alias and would line to notify an {@link Aliasable} about it.
 */
public interface HasAlias {
    void applyAliases( Aliasable aliasable );
}
