/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.randombits.confluence.metadata;

import java.util.Map;

import org.randombits.storage.Storage;

import com.atlassian.confluence.core.ContentEntityObject;

/**
 * This storage implementation provides access to metadata
 * associated with a {@link ContentEntityObject}.
 * <p/>
 * <p>This storage object is read/write, however the values
 * written are <b>not</b> saved permanently. To store permanently,
 * the storage instance must be saved via the
 * {@link org.randombits.confluence.metadata.MetadataManager#saveNextData(MetadataStorage)}
 */
public interface MetadataStorage extends Storage
{
    public static String ROW_COUNT_FIELD = "rowCount";
    
    /**
     * @return the ContentEntityObject the metadata is for.
     */
    public ContentEntityObject getContent();

    /**
     * @return the Map the data is stored in.
     */
    public Map<String, Object> getBaseMap();

    /**
     * Returns <code>true</code> if the metadata has been modified since it was
     * last loaded/created.
     *
     * @return <code>true</code> if modified.
     */
    public boolean isModified();
}
