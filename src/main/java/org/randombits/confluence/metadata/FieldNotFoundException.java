package org.randombits.confluence.metadata;

/**
 * Exception thrown when a field cannot be found.
 *
 * @author longyc
 * @since 7.2.0.20150416
 */
public class FieldNotFoundException extends Exception {

    public FieldNotFoundException() {}

    public FieldNotFoundException(String message) {
        super(message);
    }

    public FieldNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public FieldNotFoundException(Throwable cause) {
        super(cause);
    }
}
